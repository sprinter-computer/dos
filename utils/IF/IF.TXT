
IF.EXE <program.exe -key> ? trueprogram : falseprogram

; if time ...
IF -T YYYYMMDDhhmmss ? trueprogram : falseprogram

; if file exist
IF -F filename ? trueprogram : falseprogram

;if variable =
IF -V VARIABLE ? trueprogram : falseprogram


;     m h D M Y
IF -t * * 1 1 2002


ternary conditional operator: 

Another conditional operator is the "?:" (or ternary) operator, which operates as in C and many other languages. 

(expr1) ? (expr2) : (expr3);

This expression evaluates to expr2 if expr1 evaluates to TRUE, and expr3 if expr1 evaluates to FALSE. 



There is one more expression that may seem odd if you haven't seen it in other languages, the ternary conditional operator: 

$first ? $second : $third
 
If the value of the first subexpression is TRUE (non-zero), then the second subexpression is evaluated, and that is the result of the conditional expression. Otherwise, the third subexpression is evaluated, and that is the value. 

The following example 



0 = FALSE
1 = TRUE