@echo Assembling . . . 
@c:\asm\as80 -i -x3 -l -n russian.asm 
@if errorlevel=3 goto error 
@goto good 
:error 
@echo ----------------------------------------------------------------------- 
@echo �                 ERROR                        ERROR                  � 
@echo ----------------------------------------------------------------------- 
@goto quit 
:good 
@del russian.lay
@ren russian.bin russian.lay
:quit 
