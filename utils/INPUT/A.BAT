@echo Assembling . . . 
@c:\asm\as80 -i -x3 -l -n input.asm 
@if errorlevel=3 goto error 
@goto good 
:error 
@echo ----------------------------------------------------------------------- 
@echo �                 ERROR                        ERROR                  � 
@echo ----------------------------------------------------------------------- 
@goto quit 
:good 
@del input.exe 
@ren input.bin input.exe 
:quit 
