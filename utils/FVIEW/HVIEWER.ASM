
;[]===========================================================[]
;			  Hex Viewer
;	       для просмотра файлов в Hex Dump
;[]===========================================================[]
HexViewer
	LD	A,(FileShift)	;Смещение от начала кратно 16
	AND	#F0
	LD	(FileShift),A
	LD	DE,HStatusLn	 ;Печатаем низ
	CALL	PutStatLn
	LD	A,#02		;Hex проценты
	CALL	InitProcent	;Инициализ.таблицу для %
	LD	HL,ByteBuffer	;Отчищаем байтовый буфер
	LD	DE,ByteBuffer+1
	LD	BC,2460
	LD	(HL),#00
	LDIR 
	LD	HL,LineBuffer	;Отчищаем строковый буфер
	LD	B,80
	LD	A,#20
	LD	(HL),A
	INC	HL
	DJNZ	$-2
	CALL	PrnHexPage	;Печатаем страницу с текущ.смещ.
HViewLp	LD	C,#30		;Ожидаем нажатия клавиш
	RST	#10
	LD	A,E		;Е-ASCII код
	CP	#1B		;Esc
	RET	Z
	OR	A
	JR	NZ,HViewLp	;Ждем функцион.клавиши
	LD	A,D		;
	CP	#44		;F10
	RET	Z
	CP	#3E		;F4
	JP	Z,TextViewer	;Просмотр в тексте
	LD	DE,HViewLp
	PUSH	DE
	CP	#3F		;F5
	JP	Z,HGoTo		;Переход на адрес пользователя
	CP	88		;Стрелка вверх
	JP	Z,HLineUp
	CP	82		;Стрелка вниз
	JP	Z,HLineDwn
	CP	#D9		;Ctrl+PageUp
	JP	Z,HBegFile	;в начало файла
	CP	#D3		;Ctrl+PageDown
	JP	Z,HEndFile	;в конец файла
	CP	89		;PageUp
	JP	Z,HPageUp
	CP	83		;PageDown
	JP	Z,HPageDwn
	POP	DE
	JR	HViewLp
;[]===========================================================[]
;Процедура печати страницы дампа с тек.смещением от начала файла
;На вход:
;	   None
;На выход:
;	   None
PrnHexPage
	LD	HL,PrnProcent
	PUSH	HL
	LD	HL,(FileShift)
	LD	(TempShift),HL	;Запоминаем тек.смещение
	LD	DE,(FileShift+2)
	LD	(TempShift+2),DE
	LD	BC,480	;Кол-во	выводимых байт на странице
	CALL	GetBytes ;Выкачиваем байты в буфер
	LD	IX,ByteBuffer	;Байтовый буфер
	LD	DE,#0100	;Изнач.позиция печати
PrnPgLp	LD	(CurPosP+1),DE
	PUSH	IX
	LD	C,#84	;Уст.тек.знакоместо
	RST	#08
	POP	IX
	CALL	PutHLine ;Выводим строку в буфер и на экран
	JR	C,FillDwn	;CY-Закончились	байты для печати
	LD	HL,(TempShift)
	LD	DE,#0010	;Адрес дампа+16
	ADD	HL,DE
	LD	(TempShift),HL
	JR	NC,CurPosP
	LD	HL,(TempShift+2)
	INC	HL
	LD	(TempShift+2),HL
CurPosP	LD	DE,#0000
	INC	D		;След.строка
	LD	A,D
	CP	#1F		;Поз.y=31 конец	вывода
	JP	NZ,PrnPgLp
	RET 
;Процедура отчистки нижней области экрана
FillDwn	LD	DE,(CurPosP+1)	;Предид.поз.печати
	INC	D
	LD	A,#1F
	SUB	D
	RET	Z	;Z-нет отчистки
	LD	B,A
FillDw1	PUSH	DE
	PUSH	BC
	LD	C,#84
	RST	#08
	LD	BC,#5082	;Заливаем кодом	#20
	LD	A,#20
	RST	#08
	POP	BC
	POP	DE
	INC	D
	DJNZ	FillDw1
	RET 

;Процедура печати тек.строки в буфер и на экран
PutHLine
	LD	(IY+#01),#00	;Кол-во	напеч.байт
	LD	HL,(EquipBytes)
	LD	A,H
	OR	A
	LD	C,#10
	JR	NZ,PutHLn
	LD	A,L
	CP	#10
	JR	NC,PutHLn
	LD	C,L
PutHLn	LD	B,#00
	OR	A
	SBC	HL,BC
	LD	(EquipBytes),HL
	PUSH	HL
	LD	HL,LineBuffer	;буфер строки
	LD	DE,LineBuffer+#3F ;место для ASCII кодов
	INC	HL
	LD	A,(TempShift+3)	;Кладем	смещение
	CALL	PutHexNumb	;от начала фаила
	LD	A,(TempShift+2)
	CALL	PutHexNumb
	LD	A,(TempShift+1)
	CALL	PutHexNumb
	LD	A,(TempShift)
	CALL	PutHexNumb
	LD	(HL),":"
	INC	HL
	INC	HL
	LD	B,C
PutHLoop
	LD	A,(IX+#00)
	INC	IX
	LD	(DE),A
	CALL	PutHexNumb	;Процедура конверта
	INC	HL
	INC	DE
	INC	(IY+#01)	;Увелич.кол-во введенных байтов
	LD	A,(IY+#01)
	CP	#04
	JR	Z,PtHNx1
	CP	#08
	JR	Z,PtHNx1
	CP	#0C
	JR	NZ,$+3
PtHNx1	INC	HL
	DJNZ	PutHLoop

;Процедура отчистки закончившихся байтов
	LD	A,#10		;Макс.байтов
	SUB	(IY+#01)	;Тек.кол-во байтов
	JR	Z,FillE2
	LD	B,A
FillE1	LD	A,#20
	LD	(HL),A
	INC	HL
	LD	(HL),A
	INC	HL
	INC	HL
	LD	(DE),A
	INC	DE
	INC	(IY+#01)
	LD	A,(IY+#01)	;В центре 2 пробела
	CP	#04
	JR	Z,FlHNx1
	CP	#08
	JR	Z,FlHNx1
	CP	#0C
	JR	NZ,$+3
FlHNx1	INC	HL
	DJNZ	FillE1
FillE2	PUSH	IX		;Печатаем строку на экране
	LD	HL,LineBuffer
	LD	BC,#5086
	RST	#08
	POP	IX
	POP	HL
	LD	A,H
	OR	L
	RET	NZ
	SCF 
	RET		;CY-была последняя строка

;Процедура преобразования 8битовых чисел в шестнадц. текст
;На вход:
;	 HL - адрес буфера
;	  А - байт
;На выход:
;	  буфер	с текст.числом
PutHexNumb
	PUSH	AF
	RRCA 
	RRCA 
	RRCA 
	RRCA 
	CALL	GetHalf
	POP	AF
GetHalf	AND	#0F
	ADD	A,#90
	DAA 
	ADC	A,#40
	DAA 
	LD	(HL),A
	INC	HL
	RET 
;[]===========================================================[]
;Процедуры обработки View
;
;На строку вверх
;
HLineUp	LD	HL,(FileShift)
	LD	DE,(FileShift+2)
	LD	A,H		;Проверяем на начало файла
	OR	D
	OR	L
	OR	E
	RET	Z		;Z-начало файла
	LD	BC,#0010
	OR	A
	SBC	HL,BC
	LD	(FileShift),HL	;FileShift-16байт(предид.строка)
	LD	(TempShift),HL	;Запоминаем тек.смещение
	JR	NC,$+3
	DEC	DE
	LD	(FileShift+2),DE
	LD	(TempShift+2),DE
	LD	BC,#10		;Вынимаем из фаила 16 байт
	CALL	GetBytes
	LD	DE,#011E	;Скролируем экран вниз
	LD	BC,#028A
	RST	#08
	LD	DE,#0100	;Печатаем в x=0,y=1
	LD	C,#84
	RST	#08
	LD	IX,ByteBuffer	;Указатель на буфер
	CALL	PutHLine	;Создаем и печатаем строку
	CALL	PrnProcent
	RET 

;На строку вниз
HLineDwn
	LD	HL,(FileShift)
	LD	BC,(FileShift+2)
	LD	DE,480
	ADD	HL,DE
	PUSH	HL
	JR	NC,$+3		;Прибавляем к смещению страницу
	INC	BC		;байтов	(480б)
	PUSH	BC
	EX	DE,HL
	LD	HL,(FileLenght)
	OR	A
	SBC	HL,DE		;Проверяем на конец файла
	EX	DE,HL
	LD	HL,(FileLenght+2)
	SBC	HL,BC
	POP	BC
	POP	HL
	RET	C
	JR	NZ,$+5
	LD	A,D
	OR	E
	RET	Z		;Z - конец
	PUSH	HL			;Новое смещение
	PUSH	BC
	LD	HL,(FileShift)
	LD	DE,(FileShift+2)
	LD	BC,#0010
	ADD	HL,BC
	JR	NC,$+3
	INC	DE
	LD	(FileShift),HL
	LD	(FileShift+2),DE
	POP	DE
	POP	HL
	LD	(TempShift),HL	;Запоминаем тек.смещение
	LD	(TempShift+2),DE
	CALL	GetBytes	;Вынимаем из фаила 16 байт
	LD	DE,#011E	;Скролируем экран вверх
	LD	BC,#018A
	RST	#08
	LD	DE,#1E00	;Печатаем в x=0,y=30
	LD	C,#84
	RST	#08
	LD	IX,ByteBuffer	;Указатель на буфер
	CALL	PutHLine	;Создаем и печатаем строку
	CALL	PrnProcent
	RET 

;В начало файла
HBegFile
	LD	HL,(FileShift)
	LD	BC,(FileShift+2)
	LD	A,H	;Проверяем на начало файла
	OR	B
	OR	L
	OR	C
	RET	Z	;Z - Начало
	LD	HL,#0000	;Смещение = 0000
	LD	(FileShift),HL
	LD	(FileShift+2),HL
	CALL	PrnHexPage	;Печатаем страницу
	RET 

;В конец файла
HEndFile
	LD	HL,(FileShift)
	LD	BC,(FileShift+2)
	LD	DE,480
	ADD	HL,DE
	JR	NC,$+3
	INC	BC
	EX	DE,HL
	LD	HL,(FileLenght)	;Проверяем,что мы на последней
	PUSH	HL		;странице
	OR	A
	SBC	HL,DE
	EX	DE,HL
	LD	HL,(FileLenght+2)
	PUSH	HL
	SBC	HL,BC
	POP	BC
	POP	HL
	RET	C	;С - в конце
	JR	NZ,$+5
	LD	A,D
	OR	E
	RET	Z	;Z - в конце
	LD	DE,480
	OR	A
	SBC	HL,DE  ;Просчитываем смещение последней	страницы
	JR	NC,$+3
	DEC	BC
	LD	A,L	;Смещение кратно 16
	AND	#0F
	JR	Z,HEnd1
	LD	A,L
	AND	#F0
	LD	L,A
	LD	DE,#0010
	ADD	HL,DE
	JR	NC,$+3
	INC	BC
HEnd1	LD	(FileShift),HL
	LD	(FileShift+2),BC
	CALL	PrnHexPage	;Печатаем страницу
	RET 

;На страницу вверх
HPageUp	LD	HL,(FileShift)
	LD	BC,(FileShift+2)
	LD	A,H	;Проверяем на начало файла
	OR	B
	OR	L
	OR	C
	RET	Z
	LD	DE,480	;Отнимаем от тек.смещ. страницу	байтов
	SBC	HL,DE
	JR	NC,HPgUp1
	LD	A,B
	OR	C
	DEC	BC	;Проверяем,что не вывалились за
	JR	NZ,HPgUp1	;начало	файла Z	- вывалились
	INC	BC
	LD	L,C	;Тогда смещение	= 0
	LD	H,B
HPgUp1	LD	(FileShift),HL
	LD	(FileShift+2),BC
	CALL	PrnHexPage	;Печатаем страницу
	RET 

;На страницу вниз
HPageDwn
	LD	HL,(FileShift)
	LD	BC,(FileShift+2)
	LD	DE,480		;Плюсуем к тек.смещ.
	ADD	HL,DE		;страницу байтов
	PUSH	HL
	JR	NC,$+3
	INC	BC
	PUSH	BC
	EX	DE,HL
	LD	HL,(FileLenght)	   ;Проверяем,что страница
	OR	A		;последняя
	SBC	HL,DE
	EX	DE,HL
	LD	HL,(FileLenght+2)
	SBC	HL,BC
	POP	BC
	POP	HL
	RET	C	;С - нет
	JR	NZ,$+5
	LD	A,D
	OR	E
	RET	Z	;Z - нет
	LD	(FileShift),HL
	LD	(FileShift+2),BC
	CALL	PrnHexPage	;Печатаем страницу
	RET 
;[]===========================================================[]
;Процедура ввода нового	указателя смещения в файле
HGoTo	LD	HL,(FileLenght)
	LD	DE,(FileLenght+2)
	LD	A,H		;Проверяем,что длина
	OR	D		;файла больше 16 байт
	OR	E
	JR	NZ,HGoToOk
	LD	A,L
	CP	#11
	RET	C		;меньше	16байт
HGoToOk	LD	HL,NewShift	;Буфер для ввода нового	указ.
	SUB	A
	LD	(CursPos+1),A	;Позиция ввода в буфере
	INC	A
	LD	(BuffFlg+1),A
	LD	A,(FileShift+3)
	CALL	PutHexNumb	;Заносим в буфер текущее смещ.
	LD	A,(FileShift+2)	;в текстовом виде
	CALL	PutHexNumb
	LD	A,(FileShift+1)
	CALL	PutHexNumb
	LD	A,(FileShift)
	CALL	PutHexNumb
HGoToRet			;Выводим на экран буфер	и
	CALL	PrintHGT	;печатаем курсор
HGoToLp	HALT			;Ожидаем прерывание
	CALL	ChangeCurs	;проверям на смену курсора
	LD	C,#31		;Ожидаем нажатия клавиши
	RST	#10
	JR	Z,HGoToLp	;Z - не	нажата
	OR	A
	JR	Z,HGoToSys	;A=0 сист.клавиша
	CP	#0D
	JP	Z,HGTenter	;нажат Enter
	CP	#08
	JR	Z,HGTdelete	;нажат BackSpace
	CP	#1B
	JP	Z,HGTescape	;нажат Esc
	CP	#20
	JR	C,HGoToLp	;код<#20 нам не	подходит
	LD	C,A		;сохр. ASCII код
BuffFlg	LD	A,#00
	OR	A
	CALL	NZ,ClrBuff
CursPos	LD	A,#00		;тек.позиция печати в буфере
	LD	B,A		;сохр.тек.поз.
	LD	HL,NewShift	;Смещ.от начала	буфера
	ADD	A,L
	LD	L,A
	JR	NC,$+3
	INC	H
	LD	(HL),C		;Заносим код в буфер
	LD	A,B		;тек.поз.
	CP	#07		;Позиция в буфере (7-последняя)
	JR	Z,HGoToRet	;печатаем строку и курсор
	INC	A		;след.позиция
	LD	(CursPos+1),A
	JR	HGoToRet	;печатаем строку и курсор

ClrBuff	LD	HL,NewShift
	LD	B,#08
	LD	A,#20
	LD	(HL),A
	INC	HL
	DJNZ	$-2
	SUB	A
	LD	(BuffFlg+1),A
	RET 

HGTdelete			;Забой
	SUB	A
	LD	(BuffFlg+1),A
	LD	A,(CursPos+1)	;Тек.позиция в буфере
	OR	A
	JR	Z,HGoToLp	;0 - выходим
	DEC	A
	LD	(CursPos+1),A	;предид.позиция
	LD	HL,NewShift
	ADD	A,L		;смещ.в	буфере
	LD	L,A
	JR	NC,$+3
	INC	H
	LD	(HL)," "	;Забиваем пробелом
	JR	HGoToRet
HGoToSys			;системная клавиша
	LD	A,D
	CP	#54		;<-
	JR	Z,HGTleft
	CP	#56		;->
	JR	Z,HGTright
	JR	HGoToLp
HGTleft				;Курсор	влево
	SUB	A
	LD	(BuffFlg+1),A
	LD	A,(CursPos+1)	;Тек.позиция в буфере
	OR	A
	JR	Z,HGoToLp	;0 - выходим
	DEC	A
	LD	(CursPos+1),A	;Уменьш.поз.
	JP	HGoToRet
HGTright
	SUB	A
	LD	(BuffFlg+1),A
	LD	A,(CursPos+1)	;Тек.позиция в буфере
	CP	#07
	JP	Z,HGoToLp	;7 - последняя
	INC	A
	LD	(CursPos+1),A	;Увелич.поз.
	JP	HGoToRet
PrintHGT	;Процедура вывода строки и курсора
	LD	DE,#0101	;Тек.знакоместо	вывода
	LD	C,#84
	RST	#08
	LD	HL,NewShift	;Строка
	LD	BC,#0886
	RST	#08
	LD	A,(CursPos+1)	;Позиция вывода	курсора
	INC	A
	LD	E,A
	LD	D,#01
	LD	C,#84
	RST	#08
	LD	BC,#0182	;Сам курсор
	LD	A,#DB
	RST	#08
	LD	A,#01		;Номер выведенного курсора
	LD	(CurCurs+1),A
	LD	A,#06		;Ожидание для смены курсора
	LD	(CurWait+1),A
	RET 
ChangeCurs		;Процедура смены курсора
CurWait	LD	A,#00	;Задержка между	сменой курсора
	DEC	A
	LD	(CurWait+1),A
	RET	NZ	;еще не	время
	LD	HL,NewShift	;Смещ.в	буфере
	LD	A,(CursPos+1)
	LD	E,A
	INC	E
	LD	D,#01	;Тек.знакоместо
	ADD	A,L
	LD	L,A
	JR	NC,$+3
	INC	H
	LD	C,#84
	PUSH	HL
	RST	#08
	POP	HL
	LD	BC,#0182
CurCurs	LD	A,#00		;Номер текущего	курсора
	XOR	#01		;Меняем	курсор
	LD	(CurCurs+1),A
	LD	A,(HL)
	JR	Z,$+4		;Печатаем
	LD	A,#DB
	RST	#08
	LD	A,#0C		;Ожидание для смены курсора
	LD	(CurWait+1),A
	RET 
HGTenter		;Клавиша ENTER
	SUB	A		;При ошибке вернемся с курсором
	LD	(CursPos+1),A	;в нулевой позиции
	LD	BC,NewShift	;Буфер строки
	CALL	GetHexNum32	;Переводим текст в 32бит число
	JP	C,HGoToRet	;CY - синтакс.ошибка
	LD	C,E
	LD	B,D
	LD	A,L		;Адрес кратный 16
	AND	#F0
	LD	L,A
	EX	DE,HL		;Смещ.в	BC+DE
	LD	HL,(FileLenght)	;Проверяем,что смещение	в
	OR	A		;пределах файла
	SBC	HL,DE
	LD	HL,(FileLenght+2)
	SBC	HL,BC
	JR	C,HGTent	;CY-за пределами
	JR	NZ,HGTent1	;NZ-в пределах
	LD	A,D
	OR	E
	JR	NZ,HGTent1	;NZ-в пределах
HGTent	LD	HL,(FileLenght)
	LD	BC,#0010
	OR	A
	SBC	HL,BC	;смещ.=последняя строка	файла 16 байт
	EX	DE,HL
	LD	HL,(FileLenght+2)
	JR	NC,$+3
	DEC	HL
	LD	C,L
	LD	B,H
HGTent1	LD	(FileShift),DE
	LD	(FileShift+2),BC
	CALL	PrnHexPage	;Печатаем страницу
	RET 
HGTescape		;Нажат Escape
	LD	HL,NewShift	;Буфер строки
	PUSH	HL
	LD	A,(FileShift+3)	;Заносим тек.смещение
	CALL	PutHexNumb
	LD	A,(FileShift+2)
	CALL	PutHexNumb
	LD	A,(FileShift+1)
	CALL	PutHexNumb
	LD	A,(FileShift)
	CALL	PutHexNumb
	LD	DE,#0101	;Тек.знакоместо
	LD	C,#84
	RST	#08
	POP	HL
	LD	BC,#0886	;Печатаем тек смещение
	RST	#08
	RET			;Выходим
;Процедура конвертирования 32битового шестн.текстового числа
;На вход:
;	  BC - Адрес буфера с текстом (0-конец буфера)
;На выход:
;	  DE+HL	- Число	32бит
;	  CY - синтаксическая ошибка
GetHexNum32
	LD	HL,#0000
	LD	E,L
	LD	D,H
GetHNlp	LD	A,(BC)
	INC	BC
	OR	A
	RET	Z
	CP	" "
	JR	Z,GetHNlp
	CP	#61
	JR	C,$+4
	SUB	#20
	CP	#30
	RET	C
	CP	#47
	CCF 
	RET	C
	SUB	#30
	CP	#0A
	JR	C,GetHNum
	SUB	#07
	CP	#0A
	RET	C
GetHNum	ADD	HL,HL
	EX	DE,HL
	ADC	HL,HL
	EX	DE,HL
	ADD	HL,HL
	EX	DE,HL
	ADC	HL,HL
	EX	DE,HL
	ADD	HL,HL
	EX	DE,HL
	ADC	HL,HL
	EX	DE,HL
	ADD	HL,HL
	EX	DE,HL
	ADC	HL,HL
	EX	DE,HL
	OR	L
	LD	L,A
	JR	GetHNlp


