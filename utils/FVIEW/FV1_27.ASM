
;[]===========================================================[]
;			 FILE VIEWER			       ;
;			    v1.27			       ;
;	    разработан специально для Denis Commander	       ;
;    Разработка: Енин Антон	  Дата разработки: 11.01.99    ;
;		   Последняя редакция: 09.02.99		       ;
;--------------------------------------------------------------;
;							       ;
;		 Список	встроенных функций:		       ;
;  - ASCII						       ;
;  - HEX						       ;
;  - Wrap,UnWrap (в режиме ASCII)			       ;
;							       ;
;		  Добавлено в этой версии:		       ;
;  - GoTo (в режиме HEX)				       ;
;							       ;
;[]===========================================================[]
;Перед запуском	программы надо сообщить	данные о файле:
;Смещение от начала программы:
;	+3байта	- Индефикатор страниц файла
;	+4байта	- Длина	файла в	байтах
;	+8байт	- Путь к файлу и имя файла (мax	256байт,конец 0)
;[]===========================================================[]

;	ORG	#9F00
	JP	StartViewer	;Начало	программы
;[]===========================================================[]
;Данные	программы:
Indentif
	DB	#00	;Индификатор страниц памяти
FileLenght
	DS	4	;Длина файла (32бит)
FileName
	DS	#101	;Путь и	имя файла
PagesList
	DS	#100	;Список	страниц	файла
FileShift
	DS	4	;Смещение от начала файла (32бит)
TempShift
	DS	4	;Временная ячейка смещения (32бит)
NextShift
	DS	4	;Смещение след.страницы	(32бит)
LineBuffer
	DS	256	;Буфер для создания строки экрана " "
MainLine		;Верхняя строка	экрана
	DB     "View:                                   "
	DB     "            Bytes               FVv1.27 "
NStatusLn		;Нижняя	строка экрана для текста
	DB     "1       2UnWrap 3       4Hex    5       "
	DB     "6       7       8       9       10Quit  "
HStatusLn		;Нижняя	строка экрана для HEX
	DB     "1       2       3       4ASCII  5Goto   "
	DB     "6       7       8       9       10Quit  "
AtrStatLn		;Атрибуты нижней строки	экрана
;       DUP     #09
	DB	#0F,#30,#30,#30,#30,#30,#30,#30
	DB	#0F,#30,#30,#30,#30,#30,#30,#30
	DB	#0F,#30,#30,#30,#30,#30,#30,#30
	DB	#0F,#30,#30,#30,#30,#30,#30,#30
	DB	#0F,#30,#30,#30,#30,#30,#30,#30
	DB	#0F,#30,#30,#30,#30,#30,#30,#30
	DB	#0F,#30,#30,#30,#30,#30,#30,#30
	DB	#0F,#30,#30,#30,#30,#30,#30,#30
	DB	#0F,#30,#30,#30,#30,#30,#30,#30
;       EDUP 
	DB	#0F,#0F,#30,#30,#30,#30,#30,#30
ByteBuffer
	DS	2460	;буфер для копированных	байтов из файла
ProcentTab
	DS	505	;Таблица для HASH просчета процентов
			;Формат: по 5 байт
			;+0,1,2,3 - смещение для тек.процента
			;+4 - процент
ProcBuffer
	DB	"   %"	;Буфер для вывода процентов
PrintMode
	DB	#00	;Режим печати 0	- Строки по 80 символов
			;	      1	- Строки по 255	символов
WrapText
	DB	"UnWrap"	;Текст режима печати
	DB	"Wrap  "

NewShift
	DB	"        ",0	;Буфер для функции GoTo

ViewData		;IY point
ZeroFlag
	DB	#00	;для печати десят.числа
CurrentByte
	DB	#00	;кол-во	напечатанных байт в строке
EquipBytes
	DW	#0000	;кол-во	выводинмых байт
ProcFlag
	DB	#00	;Флаг вывода процентов бит 0=1-уже 100%
AddXpos			;Смещение по X при печати строки
	DB	#00	;(только для 255 символов в строке)
EmptyStr		;Кол-во	недопечатанных строк на	экране
	DB	#00	;(в конце файла)
;[]===========================================================[]
;Старт программы
StartViewer
	LD	IY,ViewData
	LD	HL,#0000	;Смещение в файле
	LD	(FileShift),HL
	LD	(FileShift+2),HL
	CALL	InitPages	;Получаем лог.номера страниц
	CALL	InitILine	;Инициализ.полосу информации
	LD	HL,#2050	;Отчищаем экран
	LD	DE,#0000
	LD	BC,#1B89
	RST	#08
	LD	DE,#0000	;Поз.x=0,y=0
	LD	C,#84
	RST	#08
	LD	HL,MainLine	;Печатаем верх
	LD	BC,#5085
	LD	E,#30		;%01101000
	RST	#08
	JP	TextViewer	;Запускаем Text	Viewer
;[]===========================================================[]
	DB	" ++File Viewer v1.27++ "
	DB	"Coded Enin Anton specially for Denis Commander"
	DB	" Last Edition:09.02.99 "
;[]===========================================================[]
;Инициализация информационной строки
InitILine
	LD	HL,FileName
	LD	DE,MainLine+6
	PUSH	DE
	PUSH	HL
	DEC	HL
	LD	B,#FF
	INC	B
	INC	HL
	LD	A,(HL)
	OR	A
	JR	NZ,$-4	;Просчитываем длину имени файла
	POP	HL
	LD	A,B	;B - длина файла
	OR	A
	LD	B,#1E
	JR	Z,InName2
	CP	#1F	;если имя <31 символа,то не обрезаем
	JR	C,InName0
	SUB	#1E	;На сколко больше
	ADD	A,L
	LD	L,A	;пропускаем буквы
	JR	NC,$+3
	INC	H
	LD	A,"."	;ставим	..
	LD	(DE),A
	INC	DE
	INC	HL
	DEC	B
	LD	(DE),A
	INC	DE
	INC	HL
	DEC	B
InName0	LD	A,(HL)		;Переносим имя
	OR	A
	JR	Z,InName1
	LD	(DE),A
	INC	HL
	INC	DE
	DJNZ	InName0
InName1	LD	A,B		;если место осталось
	OR	A
	JR	Z,InName3
InName2	LD	A,#20		;то заливаем " "
	LD	(DE),A
	INC	DE
	DJNZ	$-2
InName3	POP	HL
	LD	BC,#0023
	ADD	HL,BC
	EX	DE,HL
	LD	HL,(FileLenght)	   ;Длина файла
	EXX 
	LD	HL,(FileLenght+2)
	EXX 
	CALL	PutNumb32b	;Кладем	длину файла
	RET 
;[]===========================================================[]
;Процедура печати нижней строки	экрана с атрибутами
;На вход:
;	  DE - адрес строки
PutStatLn
	PUSH	DE
	LD	DE,#1F00	;Поз.x=0,y=31
	LD	C,#84
	RST	#08
	POP	DE
	LD	HL,AtrStatLn	;Атринбуты
	LD	BC,#5081
PutStat	PUSH	BC
	PUSH	DE
	PUSH	HL
	LD	A,(DE)
	LD	E,(HL)
	LD	B,#01
	RST	#08
	POP	HL
	INC	HL
	POP	DE
	INC	DE
	POP	BC
	DJNZ	PutStat
	RET 
;[]===========================================================[]
;Получение лог.страниц файла
InitPages
	LD	HL,PagesList	;Буфер страниц файла
	LD	A,(Indentif)	;Индефикатор блока
	LD	C,#C5
	RST	#08
	RET 
;[]===========================================================[]
;Процедура инициализация таблицы процентов
;На вход:
;	  A=0 -	проценты для режима Wrap
;	  A=1 -	проценты для режима UnWrap
;	  A=2 -	проценты для режима HexView
InitProcent
	SET	0,(IY+#04)	;Флаг вывода процентов
	OR	A
	JR	Z,InProcWr
	DEC	A
	JR	Z,InProcUWr
	JP	InProcHex
InProcWr			;Проценты для TextWrap
	LD	HL,(FileLenght)
	LD	(TempShift),HL
	LD	HL,(FileLenght+2)
	LD	(TempShift+2),HL
	LD	A,#1F
InProcW	LD	HL,(TempShift)
	LD	DE,(TempShift+2)
	LD	BC,82
	PUSH	BC
	OR	A
	SBC	HL,BC
	LD	BC,#0000
	EX	DE,HL
	SBC	HL,BC
	EX	DE,HL
	POP	BC
	RET	C
	PUSH	AF
	CALL	GetBytes
	LD	HL,ByteBuffer
	LD	BC,(EquipBytes)
	ADD	HL,BC
	LD	B,C
	CALL	SearchHome
	POP	AF
	DEC	A
	JR	NZ,InProcW
	LD	HL,(TempShift)
	LD	DE,(TempShift+2)
	JR	InitProc
InProcUWr			;Проценты для TextUnWrap
	LD	HL,(FileLenght)
	LD	(TempShift),HL
	LD	HL,(FileLenght+2)
	LD	(TempShift+2),HL
	LD	A,#1F
InProcU	LD	HL,(TempShift)
	LD	DE,(TempShift+2)
	LD	BC,#100
	PUSH	BC
	OR	A
	SBC	HL,BC
	LD	BC,#0000
	EX	DE,HL
	SBC	HL,BC
	EX	DE,HL
	POP	BC
	RET	C
	PUSH	AF
	CALL	GetBytes
	LD	HL,ByteBuffer
	LD	BC,(EquipBytes)
	ADD	HL,BC
	LD	B,C
	CALL	SearchHome
	POP	AF
	DEC	A
	JR	NZ,InProcU
	LD	HL,(TempShift)
	LD	DE,(TempShift+2)
	JR	InitProc
InProcHex			;Проценты для Hex
	LD	HL,(FileLenght)
	LD	DE,(FileLenght+2)
	LD	BC,480
	OR	A
	SBC	HL,BC
	LD	BC,#0000
	EX	DE,HL
	SBC	HL,BC
	EX	DE,HL
	RET	C
;Процедура инициализации таблицы процентов
;На вход:
;	  DE+HL	- Длина	файла-1страница
InitProc
	PUSH	HL
	PUSH	DE
	PUSH	HL
	POP	IX
	EX	DE,HL
	LD	BC,100		;Длина файла/100 (%)
	CALL	Divis32
	LD	(Coeff1+1),IX	;Получаем кол-во байт на
	LD	(Coeff2+1),DE	;один процент
	LD	IX,ProcentTab
	LD	HL,#0000
	LD	E,L
	LD	D,H
	LD	BC,#6300	;Генерим таблицу
	LD	(IX+#00),L
	INC	IX
	LD	(IX+#00),H
	INC	IX
	LD	(IX+#00),E
	INC	IX
	LD	(IX+#00),D
	INC	IX
	LD	(IX+#00),C
	INC	IX
	INC	C
CoeffLoop			;для каждого процента
	PUSH	BC
Coeff1	LD	BC,#0000
	ADD	HL,BC
	LD	(IX+#00),L
	INC	IX
	LD	(IX+#00),H
	INC	IX
	EX	DE,HL
Coeff2	LD	BC,#0000
	ADC	HL,BC
	EX	DE,HL
	POP	BC
	LD	(IX+#00),E
	INC	IX
	LD	(IX+#00),D
	INC	IX
	LD	(IX+#00),C
	INC	IX
	INC	C
	DJNZ	CoeffLoop
	POP	DE
	POP	HL
	LD	(IX+#00),L	;Макс.процент
	INC	IX
	LD	(IX+#00),H
	INC	IX
	LD	(IX+#00),E
	INC	IX
	LD	(IX+#00),D
	INC	IX
	LD	(IX+#00),C
	RES	0,(IY+#04)
	RET 
;Процедура деления (32bit)
;На вход: HL+IX/BC
;На выход:DE+IX-результат
;	     HL-остаток
Divis32	LD	A,B
	OR	C
	RET	Z
	EX	DE,HL
	LD	HL,#0000
	LD	A,#20
div32b1	ADD	IX,IX
	EX	DE,HL
	ADC	HL,HL
	EX	DE,HL
	ADC	HL,HL
	SBC	HL,BC
	JR	NC,div32b2
	ADD	HL,BC
	DEC	A
	JR	NZ,div32b1
	RET 
div32b2	INC	IX
	DEC	A
	JR	NZ,div32b1
	RET 
;[]===========================================================[]
;Процедура просчета и печати проценто
PrnProcent
	LD	HL,PutProcent
	PUSH	HL	;Выходим на печать
	LD	A,100
	BIT	0,(IY+#04)	;Уже все 100%
	RET	NZ
	LD	IX,ProcentTab+250 ;Серидина таблицы проц.50%
	LD	HL,(FileShift)	;Текущее смещение
	LD	DE,(FileShift+2)
	CALL	TestProcent	;В какой половине наход.?
	JR	C,LowTest	;<50%
	LD	A,(IX+#04)
	RET	Z		;=50%
	LD	IX,ProcentTab+375 ;Таблица с 75%
	CALL	TestProcent	;В какой половине наход.?
	JR	C,Test2		;50%<X<75%
;Процедура поиска процента вверх по таблице
Test1	LD	A,(IX+#04)	;Текущий процент
	RET	Z		;X=%
	RET	C		;X<%
	CP	100		;Конечный процент
	RET	Z
	LD	BC,#0005	;Ищем вверх по таблице
	ADD	IX,BC		;Нужный	процент
	CALL	TestProcent
	JR	Test1
;Процедура поиска процента вниз	по таблице
Test2	LD	A,(IX+#04)	;Текущий процент
	RET	Z		;X=%
	RET	NC		;X>%
	OR	A
	RET	Z
	LD	BC,-#0005	;Ищем вниз по таблице
	ADD	IX,BC		;Нужный	процент
	CALL	TestProcent
	JR	Test2

;X<50%
LowTest	LD	IX,ProcentTab+125 ;Таблица с 25%
	CALL	TestProcent	;В какой половине наход.?
	JR	C,Test2		;01%<X<25%
	JR	Test1		;25%<X<50%

;Процедура тестирования	текущего смещения
;На вход:
;	  DE+HL	- Текущее смещение
;На выход:
;	  Z - Тек.смещ=процент
;	  C - Тек.смещ<процент
;	 NC - Тек.смещ>процент
TestProcent
	PUSH	HL
	PUSH	DE
	PUSH	BC
	LD	C,(IX+#00)
	LD	B,(IX+#01)
	OR	A
	SBC	HL,BC
	EX	DE,HL
	LD	C,(IX+#02)
	LD	B,(IX+#03)
	SBC	HL,BC
	JR	NZ,$+4
	LD	A,D
	OR	E
	POP	BC
	POP	DE
	POP	HL
	RET 
;Процедура вывода процентов
PutProcent
	RES	0,(IY+#00)	;Zero flag
	LD	DE,ProcBuffer	;Буфер вывода
	PUSH	DE
	LD	L,A		;HL=процент
	LD	H,#00
	CALL	GetProc		;получить текстовое знач.
	LD	DE,#003D	;Поз.вывода
	LD	C,#84
	RST	#08
	POP	HL
	LD	BC,#0486	;Печатаем
	RST	#08
	RET 
;[]===========================================================[]
;Процедура преобразования 32битовых чисел в десятичный текст
;На вход:
;	  HL'+HL- 32битовое число
;	  DE - Адрес буфера
;На выход:
;	  буфер	с текст.числом
PutNumb32b
	RES	0,(IY+#00)
	LD	BC,#CA00
	EXX 
	LD	BC,#3B9A	;1'000'000'000
	EXX 
	CALL	GetNumb32
	LD	BC,#E100
	EXX 
	LD	BC,#05F5	;100'000'000
	EXX 
	CALL	GetNumb32
	LD	BC,#9680
	EXX 
	LD	BC,#0098	;10'000'000
	EXX 
	CALL	GetNumb32
	LD	BC,#4240
	EXX 
	LD	BC,#000F	;1'000'000
	EXX 
	CALL	GetNumb32
	LD	BC,#86A0
	EXX 
	LD	BC,#0001	;100'000
	EXX 
	CALL	GetNumb32
	LD	BC,10000	;10'000
	EXX 
	LD	BC,#0000	;100'000
	EXX 
	CALL	GetNumb32
	LD	BC,1000		;1'000
	CALL	GetNumb16
GetProc	LD	BC,100		;100
	CALL	GetNumb16
	LD	BC,10		;10
	CALL	GetNumb16
	LD	A,L
	ADD	A,"0"
	LD	(DE),A
	INC	DE
	RET 
GetNumb32
	LD	A,#2F
	OR	A
GetNm32	INC	A
	SBC	HL,BC
	EXX 
	SBC	HL,BC
	EXX 
	JR	NC,GetNm32
	ADD	HL,BC
	EXX 
	ADC	HL,BC
	EXX 
	CP	#30
	JR	Z,$+6
	SET	0,(IY+#00)
	BIT	0,(IY+#00)
	JR	NZ,$+4
	LD	A,#20
	LD	(DE),A
	INC	DE
	RET 
GetNumb16
	LD	A,#2F
	OR	A
	INC	A
	SBC	HL,BC
	JR	NC,$-3
	ADD	HL,BC
	CP	#30
	JR	Z,$+6
	SET	0,(IY+#00)
	BIT	0,(IY+#00)
	JR	NZ,$+4
	LD	A,#20
	LD	(DE),A
	INC	DE
	RET 
;[]===========================================================[]
;Процедура винимания N кол-ва байт из загруженного файла
;На вход:
;	  DE+HL	- смещение в файле 32бит
;	  BC - кол-во вынимаемых байт (max #4000)
;	  (FileLenght) - длина файла (32бит)
;	  PagesList - буфер страниц файла
;На выход:
;	  ByteBuffer - буфер с байтами
GetBytes
	IN	A,(#E2)
	PUSH	AF
	PUSH	BC
	LD	(SaveHL+1),HL	   ;Смещение от	начала
	LD	(SaveDE+1),DE
	ADD	HL,BC
	JR	NC,$+3
	INC	DE
	LD	C,L
	LD	B,H
	LD	HL,(FileLenght)
	OR	A
	SBC	HL,BC
	PUSH	HL
	LD	HL,(FileLenght+2)  ;Проверяем
	SBC	HL,DE		;Осталось столко или больше байт
	POP	DE
	POP	BC
	JR	NC,GtByte1	;NC - okey
	LD	HL,#0000
	OR	A
	SBC	HL,DE
	LD	E,L		;Иначе в BC - оставшаяся длина
	LD	D,H
	LD	L,C
	LD	H,B
	OR	A
	SBC	HL,DE
	JR	NC,$+5
	LD	HL,#0000
	LD	C,L
	LD	B,H
GtByte1	LD	(EquipBytes),BC	   ;Сохр.кол-во	байт
	LD	A,B
	OR	C
	JR	Z,TestExt
	LD	IX,PagesList	;Список	страниц	файла
SaveHL	LD	HL,#0000
SaveDE	LD	DE,#0000
	LD	A,H		;Выделяем номер	страницы
	RLA 
	RL	E
	RLA 
	RL	E
	LD	A,E
	ADD	A,XL		;IX - Адрес страницы в списке
	LD	XL,A
	JR	NC,$+4
	INC	XH
	LD	A,(IX+#00)	;Тек.страница
	OUT	(#E2),A
	LD	A,H		;Вкл.с #C000
	OR	#C0
	LD	H,A
	PUSH	HL	;Проверяем,что все нужные байты	лежат
	ADC	HL,BC	;на одной странице
	POP	HL
	JR	Z,GtByte3	;лежат
	JR	C,GtByte4	;не лежат
GtByte3	LD	DE,ByteBuffer	;лежат:	можно копировать
	LDIR 
	JR	TestExt
GtByte4	PUSH	BC		;Не лежат:
	EX	DE,HL
	LD	HL,#0000
	OR	A
	SBC	HL,DE
	LD	C,L		;BC - сколько до конца страницы
	LD	B,H
	PUSH	BC
	EX	DE,HL
	LD	DE,ByteBuffer	;копируем
	LDIR 
	POP	BC
	POP	HL
	OR	A
	SBC	HL,BC		;BC - сколько осталось
	LD	C,L
	LD	B,H
	LD	HL,#C000	;С начала след.страницы
	LD	A,(IX+#01)	;Номер след.страницы
	OUT	(#E2),A
	LDIR			;копируем
TestExt	POP	AF
	OUT	(#E2),A
	RET 
;[]===========================================================[]
	INCLUDE	"TViewer.asm"
	INCLUDE	"HViewer.asm"
;end
